/*******************************************************************/
/*                                                      actions.p4                                                           */
/*******************************************************************/

action int_transit(switch_id) {
    subtract(int_metadata.insert_cnt, int_header.max_hop_cnt, int_header.total_hop_cnt);
    modify_field(int_metadata.switch_id, switch_id);
    shift_left(int_metadata.insert_byte_cnt, int_metadata.instruction_cnt, 2);
    modify_field(int_metadata.gpe_int_hdr_len8, int_header.ins_cnt);
}

action int_reset() {
    modify_field(int_metadata.switch_id, 0);
    modify_field(int_metadata.insert_byte_cnt, 0);
    modify_field(int_metadata.insert_cnt, 0);
    modify_field(int_metadata.gpe_int_hdr_len8, 0);
    modify_field(int_metadata.gpe_int_hdr_len, 0);
    modify_field(int_metadata.instruction_cnt, 0);
}

action int_set_header_0003_i0() {
}

action int_set_header_0003_i1() {
    int_set_header_3();
}

action int_set_header_0003_i2() {
    int_set_header_2();
}

action int_set_header_0003_i3() {
    int_set_header_3();
    int_set_header_2();
}

action int_set_header_0003_i4() {
    int_set_header_1();
}

action int_set_header_0003_i5() {
    int_set_header_3();
    int_set_header_1();
}

action int_set_header_0003_i6() {
    int_set_header_2();
    int_set_header_1();
}

action int_set_header_0003_i7() {
    int_set_header_3();
    int_set_header_2();
    int_set_header_1();
}

action int_set_header_0003_i8() {
    int_set_header_0();
}

action int_set_header_0003_i9() {
    int_set_header_3();
    int_set_header_0();
}

action int_set_header_0003_i10() {
    int_set_header_2();
    int_set_header_0();
}

action int_set_header_0003_i11() {
    int_set_header_3();
    int_set_header_2();
    int_set_header_0();
}

action int_set_header_0003_i12() {
    int_set_header_1();
    int_set_header_0();
}

action int_set_header_0003_i13() {
    int_set_header_3();
    int_set_header_1();
    int_set_header_0();
}

action int_set_header_0003_i14() {
    int_set_header_2();
    int_set_header_1();
    int_set_header_0();
}

action int_set_header_0003_i15() {
    int_set_header_3();
    int_set_header_2();
    int_set_header_1();
    int_set_header_0();
}

action int_set_header_0_bos() { /* switch_id */
    modify_field(int_switch_id_header.bos, 1);
}

action int_set_header_1_bos() { /* ingress_port_id */
    modify_field(int_ingress_port_id_header.bos, 1);
}

action int_set_header_2_bos() { /* hop_latency */
    modify_field(int_hop_latency_header.bos, 1);
}

action int_set_header_3_bos() { /* q_occupancy */
    modify_field(int_q_occupancy_header.bos, 1);
}

action int_set_header_4_bos() { /* ingress_tstamp */
    modify_field(int_ingress_tstamp_header.bos, 1);
}

action int_set_header_5_bos() { /* egress_port_id */
    modify_field(int_egress_port_id_header.bos, 1);
}

action int_set_header_6_bos() { /* q_congestion */
    modify_field(int_q_congestion_header.bos, 1);
}

action int_set_header_7_bos() { /* egress_port_tx_utilization */
    modify_field(int_egress_port_tx_utilization_header.bos, 1);
}

action int_set_e_bit() {
    modify_field(int_header.e, 1);
}

action int_update_total_hop_cnt() {
    add_to_field(int_header.total_hop_cnt, 1);
}

action int_update_vxlan_gpe_ipv4() {
    add_to_field(ipv4.totalLen, int_metadata.insert_byte_cnt);
    add_to_field(udp.length_, int_metadata.insert_byte_cnt);
    add_to_field(vxlan_gpe_int_header.len, int_metadata.gpe_int_hdr_len8);
}
