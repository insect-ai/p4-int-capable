/*******************************************************************/
/*                                                     control.p4                                                            */
/*******************************************************************/
#include "headers.p4"
#include "parsers.p4"
#include "actions.p4"
#include "tables.p4"

control ingress {
    // To encapsulate or not to encapsulate? That is the question.
    if(valid(ipv4)) {
        // check lookup tables
        // if ip is "local" - no need to pass through tunnel then im the INT-SINK
        if (valid(udp) && valid(vxlan-gpe)){
            // extract INT-HEADERS + DATA and forward inner packet
        } else {
            //regular forward
        }
        // if ip is "remote" - need to pass through tunnel
        if(valid(udp) && valid(vxlan-gpe)) {
        // then if UDP+VXLAN DPORT then im INT-HOP: add my data and forward
        } else {
        // else im INT-SOURCE build INT-HEADERS + DATA
        }
    }
}

control egress {
    // snip
    apply(int_insert) {
        int_transit {
            /*
             * int_transit computes, insert_cnt = max_hop_cnt ­
             *  total_hop_cnt
             * (cannot be ­ve, not checked)
             */
            if (int_metadata.insert_cnt != 0) {
                apply(int_inst_0003);
                apply(int_inst_0407);
                apply(int_inst_0811);
                apply(int_inst_1215);
                apply(int_bos);
            }
            /* update E­bit or total_hop_cnt in the INT header */
            apply(int_meta_header_update);
        }
    }
    // snip
    if (int_metadata.insert_cnt != 0) {
        apply(int_outer_encap);
    }
}
