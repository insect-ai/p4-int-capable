/*******************************************************************/
/*                                                           parsers.p4                                                      */
/*******************************************************************/

#define VXLAN_GPE_NEXT_PROTO_INT        0x0805 mask 0x08ff
#define UDP_PORT_VXLAN_GPE              4790
#define ETHERTYPE_IPV4                  0x0800
#define UDP_PROTO_NUM                   0x11

/* start parsing ingress packets */
parser start {
    return parse_ethernet;
}

/* read ethernet header */
/* if type is ipv4 - read ipv4 header. else, continue. */
header ethernet_t ethernet;

parser parse_ethernet {
    extract(ethernet);
    return select(latest.etherType){
        ETHERTYPE_IPV4 : parse_ipv4;
        default : ingress;
    }
}

/* read ipv4 header */
header ipv4_t ipv4;

/* verify checksum */
field_list ipv4_checksum_list {
        ipv4.version;
        ipv4.ihl;
        ipv4.diffserv;
        ipv4.totalLen;
        ipv4.identification;
        ipv4.flags;
        ipv4.fragOffset;
        ipv4.ttl;
        ipv4.protocol;
        ipv4.srcAddr;
        ipv4.dstAddr;
}

field_list_calculation ipv4_checksum {
    input {
        ipv4_checksum_list;
    }
    algorithm : csum16;
    output_width : 16;
}

calculated_field ipv4.hdrChecksum  {
    verify ipv4_checksum;
    update ipv4_checksum;
}

/* if next protocol is udp, which can indicate the use of vxlan, */
/* read the udp header. else - continue. */
parser parse_ipv4 {
    extract(ipv4);
    return select(latest.protocol){
        UDP_PROTO_NUM : parse_udp;
        default : ingress;
    }
}

/* read udp header */
header udp_t udp;

/* if the destination port is a vxlan port, then parse accordingly. */
/* else, this is an ordinary udp packet so continue. */
parser parse_udp {
    extract(udp);
    set_metadata(l3_metadata.lkp_l4_sport, latest.srcPort);
    set_metadata(l3_metadata.lkp_l4_dport, latest.dstPort);
    return select(latest.dstPort) {
        /* the next line is for support for non GPE vxlan and
        is beyond the scope of the project. no parser implemented. */
        // UDP_PORT_VXLAN : parse_vxlan;
        UDP_PORT_VXLAN_GPE : parse_vxlan_gpe;
        default: ingress;
    }
}

/* read vxlan-gpe header. */
header vxlan_gpe_t vxlan_gpe;

/* if the next protocol holds the value 0x05 then next protocol is INT */
/* else, handle the encapsulated packet. */
parser parse_vxlan_gpe {
    extract(vxlan_gpe);
    set_metadata(tunnel_metadata.ingress_tunnel_type,
                 INGRESS_TUNNEL_TYPE_VXLAN_GPE);
    set_metadata(tunnel_metadata.tunnel_vni, latest.vni);
    return select (vxlan_gpe.flags, vxlan_gpe.next_proto) {
        VXLAN_GPE_NEXT_PROTO_INT : parse_gpe_int_header;
        // the parse_inner_ethernet parser is not implemented yet.
        default : parse_inner_ethernet;
    }
}

/* read the INT shim header */
header vxlan_gpe_int_header_t           vxlan_gpe_int_header;

parser parse_gpe_int_header {
    // GPE uses a shim header to preserve the next_protocol field
    extract(vxlan_gpe_int_header);
    set_metadata(int_metadata.gpe_int_hdr_len, latest.len);
    return parse_int_header;
}

/* read the INT headers */
header int_header_t                     int_header;

parser parse_int_header {
    extract(int_header);
    set_metadata(int_metadata.instruction_cnt, latest.ins_cnt);
    return select (latest.rsvd1, latest.total_hop_cnt) {
        // reserved bits = 0 and total_hop_cnt == 0
        // no int_values are added by upstream
        0x000: ingress;
        // use an invalid value below so we never transition to
        // the state
        0x100: parse_all_int_meta_value_heders;
        default: ingress;
    }
}




header int_switch_id_header_t           int_switch_id_header;
header int_ingress_port_id_header_t     int_ingress_port_id_header;
header int_hop_latency_header_t         int_hop_latency_header;
header int_q_occupancy_header_t         int_q_occupancy_header;
header int_ingress_tstamp_header_t      int_ingress_tstamp_header;
header int_egress_port_id_header_t      int_egress_port_id_header;
header int_q_congestion_header_t        int_q_congestion_header;
header int_egress_port_tx_utilization_header_t  int_egress_port_tx_utilization_header;

parser parse_all_int_meta_value_heders {
    // bogus state.. just extract all posiible int headers in the
    // correct order to build the correct parse graph for deparser
    extract(int_switch_id_header);
    extract(int_ingress_port_id_header);
    extract(int_hop_latency_header);
    extract(int_q_occupancy_header);
    extract(int_ingress_tstamp_header);
    extract(int_egress_port_id_header);
    extract(int_q_congestion_header);
    extract(int_egress_port_tx_utilization_header);
    return ingress;
}
