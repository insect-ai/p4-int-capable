/*******************************************************************/
/*                                                        tables.p4                                                           */
/*******************************************************************/
table int_insert {
    /*
     * This table is used to decide if a given device should act as
     * INT transit, INT source(not implemented) or not add INT information
     * to the packet.
     *  int_sink takes precedence over int_src
     *  {int_src, int_sink, int_header} :
     *       0, 0, 1 => transit  => insert_cnt = max­total
     *       1, 0, 0 => insert (src) => Not implemented here
     *       x, 1, x => nop (reset) => insert_cnt = 0
     *       1, 0, 1 => nop (error) (reset) => insert_cnt = 0
     *       miss (0,0,0) => nop (reset)
     */
    reads {
        int_metadata_i2e.source       : ternary;
        int_metadata_i2e.sink         : ternary;
        int_header                    : valid;
    }
    actions {
        int_transit;
        int_reset;
    }
    size : 2;
}

/*************************************************************/
/* action functions for bits 0­3 combinations, 0 is msb, 3 is lsb */
/* Each bit set indicates that corresponding INT header should be
added */


/* Table to process instruction bits 0­3 */
table int_inst_0003 {
    reads {
        int_header.instruction_mask_0003 : exact;
    }
    actions {
        int_set_header_0003_i0;
        int_set_header_0003_i1;
        int_set_header_0003_i2;
        int_set_header_0003_i3;
        int_set_header_0003_i4;
        int_set_header_0003_i5;
        int_set_header_0003_i6;
        int_set_header_0003_i7;
        int_set_header_0003_i8;
        int_set_header_0003_i9;
        int_set_header_0003_i10;
        int_set_header_0003_i11;
        int_set_header_0003_i12;
        int_set_header_0003_i13;
        int_set_header_0003_i14;
        int_set_header_0003_i15;
    }
    size : 16;
}
/* Similar table is used for instruction bit 4­7 */

/*************************************************************/
/*
 * BOS bit ­ set bottom of stack bit for the bottom most header added
 * by first hop INT device
 */


table int_bos {
    reads {
        int_header.total_hop_cnt            : ternary;
        int_header.instruction_mask_0003    : ternary;
        int_header.instruction_mask_0407    : ternary;
        int_header.instruction_mask_0811    : ternary;
        int_header.instruction_mask_1215    : ternary;
    }
    actions {
        int_set_header_0_bos;
        int_set_header_1_bos;
        int_set_header_2_bos;
        int_set_header_3_bos;
        int_set_header_4_bos;
        int_set_header_5_bos;
        int_set_header_6_bos;
        int_set_header_7_bos;
        nop;
    }
    size : 16;      /* number of instruction bits */
}

/*************************************************************/

/* update the INT metadata header */


table int_meta_header_update {
    /*
     * This table is applied only if int_insert table is a hit, which
     * computes insert_cnt
     * E bit is set if insert_cnt == 0 => cannot add INT information
     * Else total_hop_cnt is incremented by one
     */
    reads {
        int_metadata.insert_cnt : ternary;
    }
    actions {
        int_set_e_bit;
        int_update_total_hop_cnt;
    }
    size : 1;
}

/*************************************************************/



table int_outer_encap {
    /*
     * This table is applied only if it is decided to add INT info
     * as part of transit or source functionality
     * based on outer(underlay) encap, vxlan­GPE in this example,
     * update outer headers, IP/UDP total len etc.
     * {int_src, vxlan_gpe, egr_tunnel_type} :
     *      0, 0, X : nop (error)
     *      0, 1, X : update_vxlan_gpe_int (transit case)
     *      1, 0, tunnel_gpe : add_update_vxlan_gpe_int
     *      1, 1, X : add_update_vxlan_gpe_int
     *      miss => nop
     */
    reads {
        ipv4                                : valid;
        vxlan_gpe                           : valid;
        int_metadata_i2e.source             : exact;
        tunnel_metadata.egress_tunnel_type  : ternary;
    }
    actions {
        int_update_vxlan_gpe_ipv4;
        nop;
    }
    size : INT_UNDERLAY_ENCAP_TABLE_SIZE;
}
